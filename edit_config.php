<?php

$directaccess = true;

error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

require("config.php");

$r_action = (string)$_POST['action'];
$r_debug = (string)$_POST['debug'];
$r_debug_timer = (string)$_POST['debug_timer'];
$r_timezone = (string)$_POST['timezone'];
$r_longitude = (string)$_POST['longitude'];
$r_latitude = (string)$_POST['latitude'];
$r_country = (string)$_POST['country'];
$r_plz = (string)$_POST['plz'];
$r_city = (string)$_POST['city'];
$r_connairPort = (string)$_POST['connairPort'];
$r_connairIP = (string)$_POST['connairIP'];
$r_multiDeviceSleep = intval($_POST['multiDeviceSleep']);
$r_showDeviceStatus = (string)$_POST['showDeviceStatus'];
$r_showRoomButtonInDevices = (string)$_POST['showRoomButtonInDevices'];
$r_showMenuOnLoad = (string)$_POST['showMenuOnLoad'];
$r_showStatsBtnInMenu = (string)$_POST['showStatsBtnInMenu'];
$r_showMusicBtnInMenu = (string)$_POST['showMusicBtnInMenu'];
$r_showWheaterBtnInMenu = (string)$_POST['showWheaterBtnInMenu'];
$r_showTimerBtnInMenu = (string)$_POST['showTimerBtnInMenu'];
$r_showRoomsBtnInMenu = (string)$_POST['showRoomsBtnInMenu'];
$r_showGroupsBtnInMenu = (string)$_POST['showGroupsBtnInMenu'];
$r_showAllOnOffBtnInMenu = (string)$_POST['showAllOnOffBtnInMenu'];
$r_sortOrderDevices = (string)$_POST['sortOrderDevices'];
$r_sortOrderGroups = (string)$_POST['sortOrderGroups'];
$r_sortOrderRooms = (string)$_POST['sortOrderRooms'];
$r_sortOrderTimers = (string)$_POST['sortOrderTimers'];
$r_theme_mobile = (string)$_POST['theme_mobile'];
$r_theme_desktop = (string)$_POST['theme_desktop'];
$r_theme_desktop_bg = (string)$_POST['theme_desktop_bg'];
$r_db_name = (string)$_POST['db_name'];
$r_db_user = (string)$_POST['db_user'];
$r_db_password = (string)$_POST['db_password'];
$r_db_host = (string)$_POST['db_host'];
$r_timePeriodInHours = (string)$_POST['timePeriodInHours'];
$r_numSensors = (string)$_POST['numSensors'];

switch ($r_action) {
    
    case "edit":
        $xml["debug"] = $r_debug;
        $xml->timers["debug"] = $r_debug_timer;
        $xml->global->timezone = $r_timezone;
        $xml->global->longitude = $r_longitude;
        $xml->global->latitude = $r_latitude;
		$xml->global->country = $r_country;
		$xml->global->plz = $r_plz;
		$xml->global->city = $r_city;
		$xml->connairs->connair->port = $r_connairPort;
		$xml->connairs->connair->address = $r_connairIP;
        $xml->global->multiDeviceSleep = $r_multiDeviceSleep;
        $xml->gui->showDeviceStatus = $r_showDeviceStatus;
        $xml->gui->showRoomButtonInDevices = $r_showRoomButtonInDevices;
        $xml->gui->showMenuOnLoad = $r_showMenuOnLoad;
		$xml->gui->showStatsBtnInMenu = $r_showStatsBtnInMenu;
		$xml->gui->showMusicBtnInMenu = $r_showMusicBtnInMenu;
		$xml->gui->showWheaterBtnInMenu = $r_showWheaterBtnInMenu;
		$xml->gui->showTimerBtnInMenu = $r_showTimerBtnInMenu;
		$xml->gui->showRoomsBtnInMenu = $r_showRoomsBtnInMenu;
		$xml->gui->showGroupsBtnInMenu = $r_showGroupsBtnInMenu;
        $xml->gui->showAllOnOffBtnInMenu = $r_showAllOnOffBtnInMenu;
        $xml->gui->sortOrderDevices = $r_sortOrderDevices;
        $xml->gui->sortOrderGroups = $r_sortOrderGroups;
        $xml->gui->sortOrderRooms = $r_sortOrderRooms;
        $xml->gui->sortOrderTimers = $r_sortOrderTimers;
        $xml->gui->theme_mobile = $r_theme_mobile;
		$xml->gui->theme_desktop = $r_theme_desktop;
		$xml->gui->theme_desktop_bg = $r_theme_desktop_bg;
		$xml->diagramm->db_host = $r_db_host;
		$xml->diagramm->db_user = $r_db_user;
		$xml->diagramm->db_name = $r_db_name;
		$xml->diagramm->db_password = $r_db_password;
		$xml->diagramm->timePeriodInHours = $r_timePeriodInHours;
		$xml->diagramm->numSensors = $r_numSensors;
        if(check_config_global()) {
            echo "ok";
            config_save();
        }
        break;
    
    default:
        echo "action unsupported";
        break;
}


?>

