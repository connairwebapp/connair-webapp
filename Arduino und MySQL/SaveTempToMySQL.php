<?php
/***********************************************************************************
  Autor:   Enrico Sadlowski                                                        *
  Kontakt: profwebapps@gmail.com                                                   *
                                                                                   *
  PROJEKT: Arduino-MultiSensor-Temperatur-Ueberwachung                             *
  -------------------------------------------------------------------------------  *
                                                                                   *
  Arduino ruft dieses Script auf, uebergibt Innen- und Aussentemperatur            *
  (TI und TA) sowie einen Key (key), der mit dem hier angegebenen Key              *
  identisch sein muss.                                                             *
  Wenn der Key identisch ist und beide Temperaturwerte uebergeben wurden,          *
  werden die Temperaturwerte und das aktuelle Datum in der Datenbank gespeichert.  *
                                                                                   *
***********************************************************************************/
 
define("KEY","PASSWORT123");
 
include("./inc/db.inc.php");

if(isset($_GET['key']))
{
  if($_GET['key'] == KEY)
  {
    if(isset($_GET['ID1']) && isset($_GET['T1']))
    {
	  $DATUM = date("Y-m-d H:i:s");
      $ID1 = mysql_real_escape_string($_GET['ID1']);
	  $TEMP1 = mysql_real_escape_string($_GET['T1']);
	  $TEMP2 = mysql_real_escape_string($_GET['T2']);
	  $ID2 = mysql_real_escape_string($_GET['ID2']);
	  
	  $result = mysql_query("INSERT INTO arduino_temperaturen (datumzeit, ID1, temp1) 
              VALUES('".$DATUM."', '".$ID1."', '".$TEMP1."') ") or die(mysql_error());
	  
	  
 
      if(mysql_affected_rows() == 1)
      {
        $result = "Temperaturwerte gespeichert";
      } else $result = "Fehler beim speichern der Daten in der MySQL-Datenbank";
    } else $result = "Keine Temperaturwerte �bergeben";
  } else $result = "Falscher Key";
} else $result = "Kein Key �bergeben";

if(isset($_GET['key']))
{
  if($_GET['key'] == KEY)
  {
    if(isset($_GET['ID1']) && isset($_GET['T2']))
    {
	  $DATUM = date("Y-m-d H:i:s");
      $ID2 = mysql_real_escape_string($_GET['ID2']);
	  $TEMP2 = mysql_real_escape_string($_GET['T2']);
	  
	  $result = mysql_query("INSERT INTO arduino_temperaturen (datumzeit, ID1, temp1) 
              VALUES('".$DATUM."', '".$ID2."', '".$TEMP2."') ") or die(mysql_error());
	  
	  
 
      if(mysql_affected_rows() == 1)
      {
        $result = "Temperaturwerte gespeichert";
      } else $result = "Fehler beim speichern der Daten in der MySQL-Datenbank";
    } else $result = "Keine Temperaturwerte �bergeben";
  } else $result = "Falscher Key";
} else $result = "Kein Key �bergeben";
 
print_r($result);
?>