/***************************************************************************************************************************            
  PROJEKT: Temperaturlogger                                                                                                 *
  Beschreibung:  Temperaturwerte von 2 ds1820 Temperatursensoren werden an ein php-Script gesendet,                         *
  dass diese Daten in eine MySQL-Datenbank schreibt,                                                                        *
                                                                                                                            *
                                                                                                                            *
  Folgende Werte müssen angepasst werden                                                                                    *
  --------------------------------------------------------------------------------------------------------------------------*
  Intervall = Zeitraum in welchen Abständen die Temperaturdaten vom Arduino zur Datenbank übertragen werden sollen,         *
  mac[] = MAC-Adresse des Ethernet Shields                                                                                  *
  ip[] = IP-Adresse über die der Arduino erreichbar sein soll.                                                              *
  server[] = IP-Adresse des Servers auf den die Daten übertragen werden sollen. herauszufinden mit ping www.domain.de       *
  host[] = Name der Domain zu der die Verbindung aufgebaut werden soll                                                      *
  url[] = Pfad und Name der Datei die für den Upload der übergebenen Daten zur Datenbank zuständig ist.                     *
  key[] = Kennwort dass mit dem Kennwort in der php-Datei übereinstimmen muss (Sicherheitsaspekt)                           *
*****************************************************************************************************************************/
 
#include <SPI.h>
#include <Ethernet.h>             // library for ethernet functions
#include <DallasTemperature.h>    // library for temperature sensors
#include <OneWire.h>              // library for the onewire bus
 
OneWire  ds(8);                         // pin für Temperatursensoren
 
//DeviceAdressen der einzelnen ds1820 Temperatursensoren.
DeviceAddress sensor3 = { 0x28, 0x5A, 0xE5, 0x5B, 0x05, 0x00, 0x00, 0x8E };  //0x28, 0x96, 0x84, 0x5B, 0x05, 0x00, 0x00, 0xB0
DeviceAddress sensor4 = { 0x28, 0xDB, 0x67, 0x5B, 0x05, 0x00, 0x00, 0x1B };  //0x28, 0xDB, 0x67, 0x5B, 0x05, 0x00, 0x00, 0x1B
DeviceAddress sensor1 = { 0x28, 0x96, 0x84, 0x5B, 0x05, 0x00, 0x00, 0xB0 };  //0x28, 0xDB, 0x67, 0x5B, 0x05, 0x00, 0x00, 0x1B
DeviceAddress sensor5 = { 0x28, 0xDB, 0x67, 0x5B, 0x05, 0x00, 0x00, 0x1B };  //0x28, 0x5A, 0xE5, 0x5B, 0x05, 0x00, 0x00, 0x8E
DeviceAddress sensor2 = { 0x28, 0xDF, 0x89, 0x5B, 0x05, 0x00, 0x00, 0xBF };

//ETHERNET-SETTINGS
byte mac[]     = { 0x5D, 0xA2, 0xFA, 0x2D, 0x76, 0x7C };    // MAC-Adresse des Arduino
byte ip[]      = { 192, 168, 2, 10 };                     // IP-Adresse des Arduino
//byte gateway[] = { 192, 168, 178, 1 };                    // Gateway
//byte subnet[]  = { 255, 255, 255, 0 };                    // SubNet
byte server[]  = { 192, 168, 2, 47 };                     // IP-Adresse des Servers
 
EthernetClient client;
char host[]    = "ihrewaesche.de";                      // Domain
char url[]     = "/Temperatur/SaveTempToMySQL.php"; // Pfad zur PHP-Datei
char key[]     = "PASSWORT123";                     // Kennwort aus PHP-Datei
char c;                                                     // Variable für Rückgabe des Servers
 
long Interval  = 1;                                        // Upload-Interval in Minuten
DallasTemperature sensors(&ds);                                
int numSensors;                                             // Variable zum speichern der Anzahl der Temperatur-Sensoren
 
void setup()
{
  delay(1000);
 
  Serial.begin(9600); 
  Serial.flush();
  delay(200);
 
  Serial.println("Arduino TemperaturLogger");
  Serial.println("Ethernet initialisieren...");  
 
  Ethernet.begin(mac, ip);
  Interval = Interval * 1000 * 60;                            // Das in Minuten angegebene Interval in Millisekunden umrechnen
  delay(1000);                                                // warten, bis Ethernet gestartet
 
  //Sensoren abfragen
  sensors.begin();
  Serial.println("Temperatur-Sensoren ermitteln...");
 
  numSensors = sensors.getDeviceCount();                      // Anzahl der angeschlossenen Sensoren in numSensors speichern
 
  if(numSensors > 0)                                          // Es wurde mindestens 1 Sensor gefunden                                        
  {
    Serial.print(numSensors);
    Serial.println( " Temperatur-Sensoren gefunden.");
  }          
  else                                                        // Es wurde kein Sensor gefunden
  {
    Serial.println("Keine Temperatur-Sensoren gefunden.");
  }
}
 
void loop()
{
  sensors.requestTemperatures(); // Send the command to get temperatures
  float temp1 = sensors.getTempC(sensor1);                      // Temperatur von Sensor 1 ermitteln
  float temp2 = sensors.getTempC(sensor2);                      // Temperatur von Sensor 2 ermitteln
 
  Serial.print("Temp1: ");
  Serial.println(temp1);
  Serial.print("Temp2: ");
  Serial.println(temp2);
 
  Daten_senden(temp1, temp2);                                 // Temperaturdaten an Server übertragen
 
  delay(700);
 
  byte maxReads = 10; //Seconds
  while ((maxReads-- > 0) && client.connected())              // Antwort des Servers lesen
  {
    delay(1000);
    while (client.available())
    {
      char response = client.read();
      Serial.print(response);
    }
  }
  client.stop();
  Serial.println("Done.");
  client.flush();
 
  delay(Interval);
}
 
/******************************
                              *
  Daten an Server schickenn   *
                              *
*******************************/
void Daten_senden(float temp1, float temp2)
{
  if (client.connect(server, 80)) // Verbindung zum Server aufbauen
  {///Temperatur/SaveTempToMySQL.php?ID1=1&T1=0.00&ID2=2&T2=0.00&key=PASSWORT123
    Serial.println("Verbunden, Sende Daten...");
    client.print("GET " + String(url));
    Serial.print("GET " + String(url));
    client.print("?ID1=1");
    Serial.print("?ID1=1");
    client.print("&T1=");
    Serial.print("&T1=");
    client.print(temp1);
    Serial.print(temp1);
    client.print("&ID2=2");
    Serial.print("&ID2=2");
    client.print("&T2=");
    Serial.print("&T2=");
    client.print(temp2);
    Serial.print(temp2);
    client.print("&key=" + String(key));
    Serial.print("&key=" + String(key));
    client.println(" HTTP/1.1");
    Serial.println(" HTTP/1.1");
    client.print("Host: " + String(host));
    Serial.print("Host: " + String(host));
    client.println();
    Serial.println();
    client.println("User-Agent: Arduino");
    Serial.println("User-Agent: Arduino");
    client.println("Connection: close");
    Serial.println("Connection: close");
    client.println();
    Serial.println();
  }
  else
  {
    Serial.println(" ***** VERBINDUNG KANN NICHT HERGESTELLT WERDEN *****");
  }
}
 
/*************************************
                                     *
  Temperatur der Sensoren auslesen   *
                                     *
*************************************/
void writeTimeToScratchpad(byte* address)
{
  ds.reset();
  ds.select(address);
  ds.write(0x44,1);
  delay(1000);
}
 
void readTimeFromScratchpad(byte* address, byte* data)
{
  ds.reset();
  ds.select(address);
  ds.write(0xBE);
  for (byte i=0;i<9;i++)
  {
    data[i] = ds.read();
  }
}
 
float getTemperature(byte* address)
{
  int tr;
  byte data[12];
 
  writeTimeToScratchpad(address);
 
  readTimeFromScratchpad(address,data);
 
  tr = data[0];
 
  if (data[1] > 0x80)
  {
    tr = !tr + 1; //two's complement adjustment
    tr = tr * -1; //flip value negative.
  }
 
   int cpc = data[7];
   int cr = data[6];
 
   tr = tr >> 1;
 
   return tr - (float)0.25 + (cpc - cr)/(float)cpc;
}
