<?php

/************************************************
	Letztes Zeichen hinter einem String entfernen	*
************************************************/
function delLastChar($string="")
{
	$t = substr($string, 0, -1);
	return($t);
}

/****************************************************************
	Temperaturwerte, Datum, Zeit und Stundenzahl der Speicherung	* 
	dieser Werte für jeden Sensor ermitteln und in Array packen		*
****************************************************************/
function getChartValues($sensorID=0, $timePeriodInHours= 300000 )
{
	global $conn;
	$q_data  = mysqli_query( $conn, "SELECT DATE_FORMAT(datumzeit,'%H') AS STUNDE, temp1 
    	                           FROM arduino_temperaturen 
    	                           WHERE ID1 = ".$sensorID." AND datumzeit >= date_sub(now(), interval ".$timePeriodInHours." hour) and datumzeit <= now() 
    	                           GROUP BY DATE_FORMAT(datumzeit,' %Y-%m-%d %H') 
        	                       ORDER BY datumzeit ASC"); 
	$n_data = mysqli_num_rows($q_data);

	if($n_data > 0)
	{

		
		while($r_data = mysqli_fetch_array($q_data))
		{	

			$chartValues   .= $r_data['temp1'].','; // Einzelne Werte durch Komma trennen
			$stundenValues .= $r_data['STUNDE'].',';  	 // Einzelne Werte durch Komma trennen
		
		}

		$chartValues 	 = delLastChar($chartValues); 	 // Komma hinter dem letzten Temperaturwert entfernen
		$stundenValues = delLastChar($stundenValues);  // Komma hinter letzter Stunde entfernen

		return array($chartValues, $stundenValues);

	}
}

/**************************************************************
																															*
	Namen, Beschreibung, ChartType und Linienfarbe im Chart 		*
	eines Sensors-Messpunktes aus Datenbank auslesen 						*
																															* 
**************************************************************/
function getSensorSettings($sensorID=0)
{
	global $conn;
 	$q_data  = mysqli_query($conn, "SELECT mpcharttype, mplinetype, mpname, mpdescription, mplinecolor FROM arduino_messpunkte WHERE messpunktid = ".$sensorID); 
	$n_data = mysqli_num_rows($q_data);
    
	if($n_data > 0)
	{
		$r_data = mysqli_fetch_array($q_data);
		
		switch ($r_data['mpcharttype']) 
		{
    	case 1:
        $mpChartType = 'spline';
        break;
    	case 2:
        $mpChartType = 'line';
        break;
      case 3:
        $mpChartType = 'areaspline';
        break;
      case 4:
        $mpChartType = 'area';
        break;
      case 5:
        $mpChartType = 'column';
        break;    
      case 6:
        $mpChartType = 'bar';
        break;    
		}
		
		switch ($r_data['mplinetype']) 
		{
    	case 1:
        $mpLineType = 'solid';
        break;
    	case 2:
        $mpLineType = 'ShortDash';
        break;
      case 3:
        $mpLineType = 'ShortDot';
        break;
      case 4:
        $mpLineType = 'ShortDashDot';
        break;
      case 5:
        $mpLineType = 'ShortDashDotDot';
        break;    
      case 6:
        $mpLineType = 'Dot';
        break;
      case 7:
        $mpLineType = 'Dash';
        break;
      case 8:
        $mpLineType = 'LongDash';
        break;
      case 9:
        $mpLineType = 'DashDot';
        break;
      case 10:
        $mpLineType = 'LongDashDot';
        break;
      case 11:
        $mpLineType = 'LongDashDotDot';
        break;              
		}
		
		$mpName = $r_data['mpname'];
		$mpDescription = $r_data['mpdescription'];
		$mpLineColor = $r_data['mplinecolor'];
		
		return array($mpChartType, $mpLineType, $mpName, $mpDescription, $mpLineColor);
	}
}
?>